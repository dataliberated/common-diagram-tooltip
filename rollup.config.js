import postcss from "rollup-plugin-postcss";

import pkg from "./package.json";

export default [

	// CommonJS build.
	{
		input: "./src/js/main.js",
		output: {
			name: pkg.name + ".js",
			file: "./dist/" + pkg.name + ".js",
			format: "cjs",
			sourceMap: "inline"
		},
		plugins: [
			postcss()
		],
		onwarn: function(message, warn) {
			if(/deprecated/.test(message))
				return;
			warn(message);
		}
	},

	// Modern ES6 module.
	{
		input: "./src/js/main.js",
		output: {
			name: pkg.name + ".js",
			file: "./dist/" + pkg.name + ".esm.js",
			format: "esm",
			sourceMap: "inline"
		},
		plugins: [
			postcss()
		],
		onwarn: function(message, warn) {
			if(/deprecated/.test(message))
				return;
			warn(message);
		}
	}

];
