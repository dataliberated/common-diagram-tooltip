import "../css/tooltip.css";

/**
 * Creates a new tooltip element.
 * @returns {Element} Tooltip DOM element.
 */
export function create() {
	var tooltipElement = document.createElement( "div" );
	tooltipElement.id = "tooltip";
	return tooltipElement;
}

/**
 * Sets the data that an existing tooltip displays.
 * @param {Element} tooltipElement - Target tooltip element.
 * @param {String} header - Header to display at top of tooltip.
 * @param {Array} data - Array of objects containing "key" and "value" entries.
 * @example setData( myTooltip, "My ToolTip", [
 * 	{key: "Data A", value: "87.10"},
 * 	{key: "Data B", value: "Good"},
 * 	{key: "Data C", value: "£7.33"}
 * ]);
 */
export function setData( tooltipElement, header, data ) {
	clearData( tooltipElement );
	var h4 = document.createElement( "h4" );
	h4.innerHTML = header;
	tooltipElement.appendChild( h4 );

	var table = document.createElement( "table" );

	for ( var i = 0; i < data.length; i++ ) {

		var row = document.createElement( "tr" );

		var key = document.createElement( "td" );
		key.innerHTML = data[ i ].key + ":&nbsp;";
		row.appendChild( key );

		var value = document.createElement( "td" );
		value.innerHTML = data[ i ].value;
		row.appendChild( value );

		table.appendChild( row );
	}

	tooltipElement.appendChild( table );
}

/**
 * Clears all data from an existing tooltip. Called automatically by setData.
 * @param {Element} tooltipElement - Target tooltip element.
 */
export function clearData( tooltipElement ) {
	while ( tooltipElement.lastChild ) {
		tooltipElement.removeChild( tooltipElement.lastChild );
	}
}

/**
 * Sets the position of the tooltip element.
 * @param {Element} tooltipElement - Target tooltip element.
 * @param {Object} position - Object containing "x" and "y" entries.
 */
export function updatePosition( tooltipElement, position ) {
	tooltipElement.style.left = ( position.x + 16 ).toString( 10 ) + "px";
	tooltipElement.style.top = ( position.y + 16 ).toString( 10 ) + "px";
}

/**
 * Makes target tooltip element visible.
 * @param {Element} tooltipElement - Target tooltip element.
 */
export function show( tooltipElement ) {
	tooltipElement.style.display = "block";
}

/**
 * Makes target tooltip element hidden.
 * @param {Element} tooltipElement - Target tooltip element.
 */
export function hide( tooltipElement ) {
	tooltipElement.removeAttribute( "style" );
}
